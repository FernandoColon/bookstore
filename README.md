# Bookstore

Create a new app with Angular with the following  criterias:

1. Create a Login page with a hardcoded user and password, for example: **user:** admin / **password:** admin
1. The title of the app will be: **The Bookstore**.
1. If the login is wrong, display an error message specifying the credentials are wrong and not allow the user to access the app.
1. If the login is successful, let the user access to the app main screen by displaying:
	* The app title.
	* A **Logout** button.
	* An **Add Book** button (below the title).
	* A list of books (only if there's any) with the following data:
		* Author
		* Title
		* Description

## Behaviors
### Add Book
1. When the user clicks on the **Add Book** button, it should open a new view with the following content:
	* Subtitle:
		* Add New Book
	* Book form, asking for the following data:
		* Author
		* Title
		* Description
	* Action buttons:
		* Save
		* Cancel
1. All the fields are required and the user will NOT be able to save the book if any data is missing.
1. When the **Save** button has been clicked, it should save the data into an objects array and redirect the user to the main screen, displaying the list of existing books including the new one that has been added.
1. If the **Cancel** button is clicked, then the user will be redirect to the main page without saving the data from the form.

### Edit Book
1. When the user is in the main page and if there's any existing book, he/she will be able to click on the row of the desired book in order to display the following content:
	* Subtitle:
		* Edit Book
	* Book form, showing the following data (in text boxes):
		* Author
		* Title
		* Description
	* Action buttons:
		* Save
		* Delete
		* Cancel
1. All the fields are required and the user will NOT be able to save the book if any data is missing.
1. When the **Save** button has been clicked, it should update the data and redirect the user to the main screen, displaying the list of existing books including the updated data of the selected book.
1. If the **Cancel** button is clicked, then the user will be redirect to the main page without saving the data from the form.

### Delete Book
1. When the user is in the main page and if there's any existing book, he/she will be able to click on the row of the desired book in order to display the following content:
	* Subtitle:
		* Edit Book
	* Book form, showing the following data (in text boxes):
		* Author
		* Title
		* Description
	* Action buttons:
		* Save
		* Delete Book
		* Cancel
1. When the **Delete** button has been clicked, it should request for a delete confirmation (it could be a modal, a JS confirm box or whatever option you prefer).
	* If the user confirms the delete process, then it should remove the book from the objects array and redirect the user to the main screen, displaying the list of existing book excluding the deleted book.
	* If the user cancels the delete process, then the delete confirmation should be hidden.
1. If the **Cancel** button is clicked, then the user will be redirect to the main page without deleting the book.


### Logout
1. When the user clicks on the **Logout** button, it should destroy the current session and redirect the user to the login page.

### Invalid Access
1. If the user is not logged and he/she tries to access directly by using the main screen or the Book form paths, then the app should redirect him/her to the login page.
